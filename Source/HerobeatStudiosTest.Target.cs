// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class HerobeatStudiosTestTarget : TargetRules
{
	public HerobeatStudiosTestTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "HerobeatStudiosTest" } );
	}
}
