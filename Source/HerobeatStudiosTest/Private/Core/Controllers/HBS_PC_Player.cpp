// Fill out your copyright notice in the Description page of Project Settings.


#include "HBS_PC_Player.h"
#include "Core/Characters/HBS_CH_Bomberman.h"
#include "Defines.h"

AHBS_PC_Player::AHBS_PC_Player()
{
}

void AHBS_PC_Player::MoveForward(float value)
{
  if (value != 0.0f)
    ControlledBomberman->MoveForward(value);
}

void AHBS_PC_Player::MoveStrafe(float value)
{
  if(value != 0.0f)
    ControlledBomberman->MoveStrafe(value);
}

void AHBS_PC_Player::UseBomb()
{
  ControlledBomberman->DeployBomb();
}

void AHBS_PC_Player::BeginPlay()
{
  Super::BeginPlay();
}

void AHBS_PC_Player::OnPossess(APawn* aPawn)
{
  Super::OnPossess(aPawn);

  ControlledBomberman = Cast<AHBS_CH_Bomberman>(aPawn);
}

void AHBS_PC_Player::SetupInputComponent()
{
	Super::SetupInputComponent();

  check(InputComponent);

  FString ID = FString::FromInt(GetLocalPlayer()->GetControllerId());

  InputComponent->BindAction(*FString("Bomb").Append(ID), IE_Pressed, this, &AHBS_PC_Player::UseBomb);
  InputComponent->BindAxis(*FString("Forward").Append(ID), this, &AHBS_PC_Player::MoveForward);
  InputComponent->BindAxis(*FString("Strafe").Append (ID), this, &AHBS_PC_Player::MoveStrafe);
}
