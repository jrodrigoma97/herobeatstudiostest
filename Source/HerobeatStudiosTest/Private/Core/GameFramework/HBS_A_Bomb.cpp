// Fill out your copyright notice in the Description page of Project Settings.


#include "HBS_A_Bomb.h"
#include "GameFramework/DamageType.h"

#include "DrawDebugHelpers.h"

AHBS_A_Bomb::AHBS_A_Bomb()
{
	PrimaryActorTick.bCanEverTick = false;

  Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));

  SetRootComponent(Mesh);
}

void AHBS_A_Bomb::Explode()
{
  FVector Location = GetActorLocation();

  for (FVector dir : ExplosionDirections)
  {
    DrawDebugLine(GetWorld(), Location, Location + (dir * 300.0f), FColor::Red, false, 1.0f, 0, 10.0f);
  }
  Destroy();
}

void AHBS_A_Bomb::DeployBomb(FBombData DataToCreateBomb)
{
  Data = DataToCreateBomb;

  GetWorldTimerManager().SetTimer(ExplosionTimerHandle,
                                  this, &AHBS_A_Bomb::Explode,
                                  0.1f, false, Data.ExplosionDelay);
}

void AHBS_A_Bomb::BeginPlay()
{
	Super::BeginPlay();
	
}

