// Fill out your copyright notice in the Description page of Project Settings.


#include "HBS_CH_Bomberman.h"
#include "GameFramework/CharacterMovementComponent.h"

AHBS_CH_Bomberman::AHBS_CH_Bomberman()
{
	PrimaryActorTick.bCanEverTick = true;

}

void AHBS_CH_Bomberman::MoveForward(float value)
{
  GetCharacterMovement()->AddInputVector(FVector(value, 0.0f, 0.0f));
}

void AHBS_CH_Bomberman::MoveStrafe(float value)
{
  GetCharacterMovement()->AddInputVector(FVector(0.0f, value, 0.0f));
}

void AHBS_CH_Bomberman::DeployBomb()
{
  const FVector Location = GetActorLocation();

  AHBS_A_Bomb* Bomb = GetWorld()->SpawnActor<AHBS_A_Bomb>(Data.BombType, Location, FRotator::ZeroRotator);

  Bomb->DeployBomb(Data.BombData);
}

void AHBS_CH_Bomberman::BeginPlay()
{
	Super::BeginPlay();
	
}

float AHBS_CH_Bomberman::TakeDamage(float Damage,
                                   FDamageEvent const& DamageEvent,
                                   AController* EventInstigator, 
                                   AActor* DamageCauser)
{
  Damage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

  if (Damage == 0.0f)
  {
    return 0.f;
  }



  return Damage;
}

