// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HBS_A_Bomb.generated.h"

class UDamageType;
class UStaticMeshComponent;

USTRUCT(BlueprintType)
struct FBombData
{
  GENERATED_BODY()

   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bomb", meta=(ClampMin=0))
    float ExplosionDelay;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bomb")
    TSubclassOf<UDamageType> DamageType;

};

UCLASS(notplaceable)
class HEROBEATSTUDIOSTEST_API AHBS_A_Bomb : public AActor
{
	GENERATED_BODY()
	
public:	

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bomb")
    TArray<FVector> ExplosionDirections;

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Bomb")
    UStaticMeshComponent* Mesh;

	AHBS_A_Bomb();

  UFUNCTION(BlueprintCallable, Category="Bomb")
  virtual void Explode();

  UFUNCTION(BlueprintCallable, Category = "Bomb")
    virtual void DeployBomb(FBombData Data);

protected:
	virtual void BeginPlay() override;

  
  FBombData    Data;
  FTimerHandle ExplosionTimerHandle;
};
