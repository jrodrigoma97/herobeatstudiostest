// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "HBS_PC_Player.generated.h"

class AHBS_CH_Bomberman;

/**
 *
 */
UCLASS()
class HEROBEATSTUDIOSTEST_API AHBS_PC_Player : public APlayerController
{
  GENERATED_BODY()

 protected:
   AHBS_CH_Bomberman* ControlledBomberman;

 private:

    AHBS_PC_Player();

 protected:
  UFUNCTION()
    virtual void MoveForward(float value);

  UFUNCTION()
    virtual void MoveStrafe(float value);

  UFUNCTION()
    virtual void UseBomb();
 private:

  virtual void BeginPlay() override;

  virtual void OnPossess(APawn* aPawn) override;

  virtual void SetupInputComponent() override;

};
