// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/HBS_A_Bomb.h"
#include "HBS_CH_Bomberman.generated.h"


USTRUCT(BlueprintType)
struct FBombermanData
{
  GENERATED_BODY()

  UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(ClampMin=0))
    int MaxPlaceableBombs = 0;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = 0))
    int BlastDistance = 0;

  UPROPERTY()
    int CurrentActiveBombs;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSubclassOf<AHBS_A_Bomb> BombType;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FBombData BombData;

};

UCLASS()
class HEROBEATSTUDIOSTEST_API AHBS_CH_Bomberman : public ACharacter
{
	GENERATED_BODY()

    /*********************
    |     VARIABLES      |
    *********************/
public:

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bomberman")
    FBombermanData Data;

    /*********************
    |     FUNCTIONS      |
    *********************/

	AHBS_CH_Bomberman();

  UFUNCTION(BlueprintCallable, Category="Bomberman")
    virtual void MoveForward(float value);

  UFUNCTION(BlueprintCallable, Category = "Bomberman")
    virtual void MoveStrafe(float value);

  UFUNCTION(BlueprintCallable, Category = "Bomberman")
    virtual void DeployBomb();

protected:
	virtual void BeginPlay() override;

  virtual float TakeDamage(float Damage,
                          FDamageEvent const& DamageEvent,
                          AController* EventInstigator,
                          AActor* DamageCauser) override;
};
